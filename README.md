
# Proyecto Lumen 10

> Es importante comenzar por el proyecto Vue-3 antes de abordar Lumen-10

El objetivo de este proyecto es crear una imagen Docker para un proyecto Lumen v10 haciendo uso de la [documentación oficial](https://lumen.laravel.com/docs/10.x).

## 1. Imagen para desarrollo

Crea un fichero `Dockerfile.dev` para las instrucciones de creación de una imagen Docker orientada al desarollo.

## 2. Imagen producción

Crea un fichero `Dockerfile.prod` para las instrucciones de creación de una imagen Docker orientada al desarollo. 

## 3. Mismo Dockerfile para desarrollo y producción.

Crear un fichero `Dockerfile` que no sirva para producción y desarrollo.